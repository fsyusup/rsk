
const express = require("express");
const bodyParser = require("body-parser");

//load .env
require('dotenv').config()

const app = express();

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.json({ message: "Backend test RSK." });
});


var cors = require('cors');
app.options('*', cors())
app.use(cors());


var cors = require('cors')



require("./app/routes/company.routes.js")(app);

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});