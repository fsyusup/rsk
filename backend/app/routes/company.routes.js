
module.exports = app => {
    const companies = require("../controllers/company.controller.js");
    app.post("/companies", companies.create);
    app.get("/companies", companies.findAll);
    app.get("/companies/:companyId", companies.findOne);
    app.put("/companies/:companyId", companies.update);
    app.delete("/companies/:companyId", companies.delete);
  };